
import React, { Component } from 'react';
  import Players from './Players';
// import './App.css';

class Teams extends Component {

    constructor(){
        super();
        this.state={
            flag:false,
            // teams:props.names,
            teams:["RCB","CSK","KNR","DC","KXIP"],
            year:props.year,
             teamName:[]         
    }
}
   
    // showPlayers=(team)=>{
    //     if(this.state.visible !== false && this.state.teamName!==team){
    //     this.setState({
    //        teamName:team,
    //       flag:true
    //     });
    // }
    // else{

    //     this.setState({
    //         teamName:team,
    //        flag:!this.state.visible
    //      });
    // }
    //     console.log(this.state.visible); 
    //   }


    showPlayers=(team)=>{        
    //     if(this.state.visible !== false && this.state.teamName!==team){
    //     this.setState({
    //      preVisible:this.state.flag,
    //        teamName:team,
    //       flag:true
           
    //     });
    // }

    //     else{
    //     this.setState({
    //          preVisible:this.state.flag,
    //         teamName:team,
    //        flag:!this.state.flag
    //      });
    // }

    if(!this.state.teamName.includes(team)){
        this.state.teamName.push(team);     
        this.setState({
            teamName: this.state.teamName
        }); 
        }
        else{
            let index = this.state.teamName.indexOf(team);
            this.state.teamName.splice(index, 1);
            // this.state.teamName.pop(team);
            this.setState({
                teamName: this.state.teamName
            });
        }   
       
         updateChildStateToParent();
      
      }

      updateChildStateToParent() {
        this.props.handleOpenedChildrenElements(this.state.year,this.state.teamName);            
    }

 // let elements = this.props.seasons.map((season) => {
    //     return (<li><button onClick={this.showTeams}>{season}</button></li>)
    // })
  render() {
   
    let elements = this.state.teams.map((team) => {
        return (<li key={team}><button onClick={this.showPlayers.bind(this,team)}>{team}</button> {(this.state.teamName.includes(team))?<Players/>:null}</li>)
    })

    return (   
    <ul>{elements}</ul>
    );
  }
}


//another way to set default props
// Seasons.defaultProps={
//         seasons:[2008,2009,2010,2011,2012,2013,2014,2015,2016,2017]
//     }

export default Teams;


